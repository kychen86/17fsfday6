//IIFE
(function(){

    //Create a module
    var MyApp = angular.module("MyApp",[]); // the string is the name in html not the var.

    var MyCtrl = function(){
        var myCtrl = this;

        myCtrl.message = "hello, world Day6";
    };
    // Use the function as a controller
    MyApp.controller("MyCtrl", MyCtrl);



})();