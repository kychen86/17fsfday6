var express = require("express");
var app = express();

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));



var port = parseInt(process.argv[2]) || 3000
app.listen(port,function(){
    console.log("Application started at port %d", port);
})